angular.module('WelldoneApp.controllers', [])

.controller('RootCtrl', function(Locations, Categories, Modal, Sharing, $state, Map) {

    var openModal = Modal.open;

    var openLocationDetailsModal = function(item) {
        openModal('templates/location.html', {
            item: item
        });
    }

    var openCategoryDetailsModal = function(item) {
        openModal('templates/category.html', {
            item: item
        });
    }

    this.sharing = Sharing.share;

    this.closeModal = Modal.close;

    this.addLocation = function() {
        if (Categories.get().length == 0) {
            $state.go('tab.categories');
            return;
        }
        openLocationDetailsModal(Locations.create());
    }

    this.editLocation = function(id) {
        openLocationDetailsModal(angular.copy(Locations.get(+id)));
    }

    this.addCategory = function() {
        openCategoryDetailsModal(Categories.create());
    }

    this.editCategory = function(id) {
        openCategoryDetailsModal(angular.copy(Categories.get(+id)));
    }

    this.gotoLocation = function(location) {
        $state.go("tab.map");
        Map.setCenter(location.coordinates);
    }
})

.controller('MapCtrl', function(Locations, Map) {

    this.locations = Locations.get();
    Map.init();
})

.controller('LocationsListCtrl', function(Locations, Categories) {

    this.locations = Locations.get();
    this.remove = Locations.remove;
    this.getCategoryById = Categories.get;
    this.categories = Categories.get();

    this.hasCrossCategories = function(arr1, arr2) {
        for (var i = 0, len = arr1.length; i < len; i++) {
            if (arr2[arr1[i]]) {
                return true;
            }
        }
        return false;
    }
})

.controller('CategoriesListCtrl', function(Categories) {

    this.categories = Categories.get();
    this.remove = Categories.remove;
})

.controller('LocationEditorCtrl', function(Locations, Categories) {

    this.saveItem = Locations.saveItem;
    this.categories = Categories.get();

    this.isCategoryChosen = function(location) {
        if (!location) return false;
        var cats = location.categories.filter(function(item) {
            return item != null
        });
        return cats.length > 0;
    }

    this.categoryChanged = function(item, category, value) {
        if (value && !this.hasCategory(item, category.id)) item.categories.push(category.id);
        else {
            item.categories.splice(item.categories.indexOf(category.id), 1);
        }
    }

    this.hasCategory = function(item, catId) {
        return item.categories.indexOf(catId) >= 0;
    }

})

.controller('CategoryEditorCtrl', function(Categories) {

    this.saveItem = Categories.saveItem;
})

;
