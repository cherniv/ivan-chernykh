angular.module('WelldoneApp.services')

.factory('Categories', function($localStorage, UniqueId) {

    var cats = $localStorage.getArray('categories');

    var newCat = {
        name: ""
    }

    var checkForDuplicates = function(name) {
        return cats.filter(function(item) {
            return item.name == name
        }).length > 0;
    }

    var save = function() {
        $localStorage.set('categories', cats);
    }

    var service = {
        get: function(id) {
            if (angular.isDefined(id)) {
                return cats.filter(function(item) {
                    return item.id == id
                })[0];
            }
            return cats;
        },
        create: function() {
            return angular.copy(angular.extend(newCat, {
                id: UniqueId.get()
            }));
        },
        remove: function(item) {
            cats.splice(cats.indexOf(item), 1);
            save();
        },
        saveItem: function(editedItem) {
            if (checkForDuplicates(editedItem.name)) return;

            var item = service.get(editedItem.id);
            if (!item) cats.unshift(editedItem);
            else angular.extend(item, editedItem);

            save();
        }
    };

    return service;
});
