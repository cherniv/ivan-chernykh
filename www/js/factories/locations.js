angular.module('WelldoneApp.services')

.factory('Locations', function($localStorage, UniqueId, $rootScope) {

    var locs = $localStorage.getArray('locations');

    var newLoc = {
        name: "",
        address: "",
        coordinates: {
            x: null,
            y: null
        },
        categories: []
    }

    var save = function() {
        $localStorage.set('locations', locs);
    }

    var service = {
        get: function(id) {
            if (angular.isDefined(id)) {
                return locs.filter(function(item) {
                    return item.id == id
                })[0];
            }
            return locs;
        },
        remove: function(item) {
            locs.splice(locs.indexOf(item), 1);
            $rootScope.$broadcast("LOCATION_REMOVED", item);
            save();
        },
        create: function() {
            return angular.copy(angular.extend(newLoc, {
                id: UniqueId.get()
            }));
        },
        saveItem: function(editedItem) {
            var item = service.get(editedItem.id);
            if (!item) {
                locs.unshift(editedItem);
                $rootScope.$broadcast("NEW_LOCATION_ADDED", editedItem);
            } else {
                angular.extend(item, editedItem);
                $rootScope.$broadcast("LOCATION_EDITED", editedItem);
            }
            save();

        }
    };

    return service;
});
