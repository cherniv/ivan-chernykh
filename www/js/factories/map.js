angular.module('WelldoneApp.services')

.factory('Map', function($cordovaGeolocation, Locations, Categories, Modal, $state, $rootScope, Marker) {

    var latLng;
    var gmap;
    var locations = Locations.get();
    var markers = {};
    var geolocationSettings = {
        timeout: 10000,
        enableHighAccuracy: true
    };
    var mapSettings = {
        center: new google.maps.LatLng(-33, 151),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var gmapEl;

    var setCenter = function(coordinates) {
        gmap.setCenter(new google.maps.LatLng(coordinates.x, coordinates.y))
    }

    var removeMarker = function(id) {
        markers[id].remove();
        markers[id] = null;
    }

    var placeMarker = function(location) {
        var marker = Marker.create(gmap, location);
        markers[location.id] = marker;
    }

    var placeMarkers = function() {
        for (var i = 0, len = locations.length; i < len; i++) {
            placeMarker(locations[i]);
        }
    }

    var onMapClick = function(event) {
        if (Categories.get().length == 0) {
            $state.go('tab.categories');
            return;
        }
        mapZoom = gmap.getZoom();
        startLocation = event.latLng;
        setTimeout(function() {
            if (mapZoom == gmap.getZoom()) {
                var newLocation = Locations.create();
                newLocation.coordinates = {
                    x: startLocation.H,
                    y: startLocation.L
                };
                Modal.open('templates/location.html', {
                    item: newLocation
                });
            }
        }, 300);
    }

    var init = function() {
        gmapEl = document.getElementById("gmap")
        gmap = new google.maps.Map(gmapEl, mapSettings);

        // Fix for grey areas on the map 
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            if (toState.name == "tab.map") {
                setTimeout(function() {
                    google.maps.event.trigger(gmap, "resize");
                }, 15);
            }
        });

        google.maps.event.addListenerOnce(gmap, 'idle', placeMarkers);

        google.maps.event.addListener(gmap, 'click', onMapClick);

        $cordovaGeolocation.getCurrentPosition(geolocationSettings).then(function(position) {
            latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            gmap.setCenter(latLng);
        }, function(error) {
            console.log("Geolocation detection problem");
        });
    }

    $rootScope.$on("NEW_LOCATION_ADDED", function(e, data) {
        placeMarker(data)
    });

    $rootScope.$on("LOCATION_EDITED", function(e, data) {
        removeMarker(data.id);
        placeMarker(data)
    });

    $rootScope.$on("LOCATION_REMOVED", function(e, data) {
        removeMarker(data.id);
    });

    return {
        init: init,
        placeMarker: placeMarker,
        setCenter: setCenter
    }
});
