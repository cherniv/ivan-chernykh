angular.module('WelldoneApp.services')

.factory('Marker', function() {

    var Marker = function(gmap, location) {

        var latLng = new google.maps.LatLng(location.coordinates.x, location.coordinates.y);
        var marker = new google.maps.Marker({
            map: gmap,
            animation: google.maps.Animation.DROP,
            position: latLng
        });

        var clickHandler = function() {

            var infoWindowSetings = {
                content: '<b>' + location.name + '</b><br>' + location.address
            };
            var infowindow = new google.maps.InfoWindow(infoWindowSetings);

            gmap.setCenter(marker.getPosition());
            infowindow.open(gmap, marker);

            navigator.vibrate(1000);
        }

        marker.addListener('click', clickHandler);

        this.remove = function() {
            marker.setMap(null);
        }
    }

    return {
        create: function(gmap, location) {
            return new Marker(gmap, location);
        }
    }
})

;
