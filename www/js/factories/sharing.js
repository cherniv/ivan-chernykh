angular.module('WelldoneApp.services')

.factory('Sharing', function($cordovaSocialSharing) {
    return {
        share: function() {
            var message = "Checkout this cool app!";
            var subject = "Hi!";
            var file;
            var link = "http://www.bioludus.com";
            $cordovaSocialSharing
                .share(message, subject, file, link) // Share via native share sheet
                .then(function(result) {
                    // Success!
                }, function(err) {
                    // An error occured. Show a message to the user
                })
        }
    }
});
