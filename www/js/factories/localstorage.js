angular.module('WelldoneApp.services')

.factory('$localStorage', function($window) {

    return {
        set: function(key, value) {
            if (typeof value == "object") {
                $window.localStorage[key] = JSON.stringify(value);
            } else {
                $window.localStorage[key] = value;
            }
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        getArray: function(key) {
            return JSON.parse($window.localStorage[key] || '[]');
        },
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
});
