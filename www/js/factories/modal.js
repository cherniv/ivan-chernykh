angular.module('WelldoneApp.services')

.factory('Modal', function($ionicModal, $rootScope) {

    var modal;
    var modalScope;

    return {
        open: function(tplUrl, data) {
            modalScope = $rootScope.$new();
            $ionicModal.fromTemplateUrl(tplUrl, {
                scope: angular.extend(modalScope, data)
            }).then(function(_modal) {
                modal = _modal;
                modal.show();
            });
        },
        close: function() {
            modal.hide();
        }
    }
})
