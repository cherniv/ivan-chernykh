angular.module('WelldoneApp.services')

.factory('UniqueId', function() {
    return {
        get: function() {
            return new Date().getTime();
        }
    }
});
