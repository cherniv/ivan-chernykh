angular.module('WelldoneApp.directives', [])

.directive("body", function() {
    return {
        restrict: "E",
        controller: "RootCtrl",
        controllerAs: "root"
    }
})

.directive("locationDetailsWindow", function() {
    return {
        restrict: "C",
        controller: "LocationEditorCtrl",
        controllerAs: "editor"
    }
})

.directive("categoryDetailsWindow", function() {
    return {
        restrict: "C",
        controller: "CategoryEditorCtrl",
        controllerAs: "editor"
    }
})

.directive("mainNavBar", function() {
    return {
        priority: 100000,
        restrict: "E",
        templateUrl: "templates/main-nav-bar.html"
    }
})

;
