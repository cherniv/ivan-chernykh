// myLocations exam app 
// By Ivan Chernykh

angular.module('WelldoneApp.services', []);

angular.module('WelldoneApp', ['ionic', 'ngCordova', 'WelldoneApp.controllers', 'WelldoneApp.services', 'WelldoneApp.directives'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }
    });
})

.config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider) {

    $ionicConfigProvider.tabs.position('bottom');

    $stateProvider

        .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    .state('home', {
        url: '/',
        templateUrl: 'templates/home.html'
    })

    .state('tab.map', {
        url: '/map',
        views: {
            'tab-map': {
                templateUrl: 'templates/tab-map.html',
                controller: 'MapCtrl as map'
            }
        }
    })

    .state('tab.locations', {
        url: '/locations',
        views: {
            'tab-locations': {
                templateUrl: 'templates/tab-locations.html',
                controller: 'LocationsListCtrl as locationsList'
            }
        }
    })

    .state('tab.categories', {
        url: '/categories',
        views: {
            'tab-categories': {
                templateUrl: 'templates/tab-categories.html',
                controller: 'CategoriesListCtrl as categoriesList'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');

});
